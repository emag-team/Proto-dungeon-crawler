/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/js/game.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/js/game.js":
/*!************************!*\
  !*** ./src/js/game.js ***!
  \************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _player__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./player */ \"./src/js/player.js\");\n/* harmony import */ var _mob__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./mob */ \"./src/js/mob.js\");\n/* harmony import */ var _level__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./level */ \"./src/js/level.js\");\n\r\n\r\n\r\n\r\n\r\n\r\nlet player;\r\nlet game = [];\r\nlet ennemy = new _mob__WEBPACK_IMPORTED_MODULE_1__[\"Mob\"]();\r\n\r\n\r\nlet stage = 5;\r\nlet forest = new _level__WEBPACK_IMPORTED_MODULE_2__[\"Level\"](stage);\r\nlet hard = 1;\r\nlet land = \"\";\r\n\r\nfunction start() {\r\n  drawGrid(game);\r\n  console.log(\"to finish the stage : gain 10 level /////// test modif guigui!\");\r\n\r\n  document.removeEventListener('keydown', start);\r\n  land = \"wood\"\r\n  game = forest.generate();\r\n  player = new _player__WEBPACK_IMPORTED_MODULE_0__[\"Player\"](\"Monsterbrau\", 0, 0, 7, 65, 2, 5, 5, 0, 0, 0, [], 55, stage);\r\n  game[player.x][player.y];\r\n  console.log(game);\r\n  player.toDraw();\r\n  console.log(\"use z,q,s,d, keys in your keyboard to move into the dungeon \");\r\n}\r\n\r\nfunction toMove(event) {\r\n  gainLevel();\r\n  if (event.keyCode === 90) {\r\n    player.moveUp();\r\n    board.innerHTML = \"\";\r\n    drawGrid(game);\r\n  } else if (event.keyCode === 83) {\r\n    player.moveDown();\r\n    board.innerHTML = \"\";\r\n    drawGrid(game);\r\n  } else if (event.keyCode === 81) {\r\n    player.moveLeft();\r\n    board.innerHTML = \"\";\r\n    drawGrid(game);\r\n  } else if (event.keyCode === 68) {\r\n    player.moveRight();\r\n    board.innerHTML = \"\";\r\n    drawGrid(game);\r\n  }\r\n\r\n  console.log(game[player.x][player.y]);\r\n\r\n  if ((game[player.x][player.y].randomEvent) === 'treasure') {\r\n    treasure();\r\n  }\r\n\r\n  if (game[player.x][player.y].randomEvent === \"ennemy\") {\r\n    console.log(`you have encounter an ennemy !`);\r\n    document.removeEventListener('keydown', toMove);\r\n    console.log('press enter to begin the fight');\r\n    document.addEventListener('keydown', toFight);\r\n  }\r\n\r\n  if (game[player.x][player.y].randomEvent === \"enygma\") {\r\n    enygma();\r\n  }\r\n\r\n  if (game[player.x][player.y].randomEvent === \"trap\") {\r\n    trap();\r\n  }\r\n\r\n  if (game[player.x][player.y].randomEvent === 'trapped-treasure') {\r\n    trapppedTreasure();\r\n  }\r\n\r\n  if (game[player.x][player.y].randomEvent === 'boss') {\r\n    document.removeEventListener('keydown', toMove);\r\n    console.log('press enter to begin the fight');\r\n    document.addEventListener('keydown', toFight);\r\n  }\r\n\r\n  if (game[player.x][player.y].randomEvent === 'armory') {\r\n    armory();\r\n  }\r\n  if (game[player.x][player.y].randomEvent === 'sword master') {\r\n    swordMaster();\r\n  }\r\n  if (game[player.x][player.y].randomEvent === 'rogue master') {\r\n    rogueMaster();\r\n  }\r\n  if (game[player.x][player.y].randomEvent === 'thaumaturge') {\r\n    thaumaturge();\r\n  }\r\n}\r\n\r\nfunction mobGeneration() {\r\n  let mobName = nameMobGenerator();\r\n  let percent = Math.floor(Math.random() * hard + 45);\r\n  let num = Math.floor(Math.random() * hard * 2);\r\n  if (num === 0) {\r\n    num += hard;\r\n  }\r\n  let mob = new _mob__WEBPACK_IMPORTED_MODULE_1__[\"Mob\"](mobName, num, percent, num, num, num);\r\n  return mob;\r\n}\r\n\r\nfunction bossGeneration() {\r\n  let mobName = nameBossGenerator(land);\r\n  let percent = Math.floor(Math.random() * hard + 65);\r\n  let num = Math.floor(Math.random() * hard * 5);\r\n  if (num === 0) {\r\n    num += 5;\r\n  }\r\n  let boss = new _mob__WEBPACK_IMPORTED_MODULE_1__[\"Mob\"](mobName, num, percent, num, num, num);\r\n  boss.boss = true;\r\n  return boss;\r\n}\r\n\r\nfunction firstStrike() {\r\n  console.log(`you have first strike !`);\r\n  let dd = 100;\r\n  let attack = Math.floor(Math.random() * dd);\r\n  if (attack <= player.force) {\r\n    console.log(`attack success with ${attack} !`);\r\n    let hit = player.totalAttack() - ennemy.armor;\r\n    if (hit <= 0) {\r\n      hit = 1\r\n    }\r\n    ennemy.pv -= hit;\r\n    dd -= 10;\r\n    console.log(`you have made ${hit} dommages !`);\r\n    ennemy.mobDeath();\r\n  } else {\r\n    console.log('attack fail !');\r\n    let dd = 100;\r\n    let attack = Math.floor(Math.random() * dd);\r\n    if (attack <= ennemy.force) {\r\n      console.log(`${ennemy.name} attack success with ${attack} !`);\r\n      let hit = ennemy.dg - player.totalArmor();\r\n      if (hit <= 0) {\r\n        hit = 1\r\n      }\r\n      player.pv -= hit;\r\n      dd -= 10\r\n      console.log(`you have take ${hit} dommages !`);\r\n      player.death();\r\n    } else {\r\n      console.log(`${ennemy.name}'s attack fail !`);\r\n    }\r\n  };\r\n\r\n}\r\n\r\nfunction secondStrike() {\r\n  console.log(`${ennemy.name} first strike !`);\r\n  let dd = 100;\r\n  let attack = Math.floor(Math.random() * dd);\r\n  if (attack <= ennemy.force) {\r\n    console.log(`${ennemy.name} attack success with ${attack} !`);\r\n    let hit = ennemy.dg - player.totalArmor();\r\n    if (hit <= 0) {\r\n      hit = 1\r\n    }\r\n    player.pv -= hit;\r\n    dd -= 10;\r\n    console.log(`you have take ${hit} dommages !`);\r\n    player.death();\r\n  } else {\r\n    console.log(`${ennemy.name}'s attack fail !`);\r\n    let dd = 100;\r\n    let attack = Math.floor(Math.random() * dd);\r\n    if (attack <= player.force) {\r\n      console.log(`attack success with ${attack} !`);\r\n      let hit = player.totalAttack() - ennemy.armor;\r\n      if (hit <= 0) {\r\n        hit = 1\r\n      }\r\n      ennemy.pv -= hit;\r\n      dd -= 10;\r\n      console.log(`you have made ${hit} dommages !`);\r\n      ennemy.mobDeath();\r\n    } else {\r\n      console.log('attack fail !');\r\n    }\r\n  }\r\n}\r\n\r\nfunction toFight(event) {\r\n  if (event.keyCode === 13) {\r\n    if (game[player.x][player.y].randomEvent === 'boss') {\r\n      ennemy = bossGeneration();\r\n    } else { ennemy = mobGeneration(hard); };\r\n    console.log(ennemy);\r\n    while (player.pv > 0 && ennemy.pv > 0) {\r\n      if (player.init >= ennemy.init) {\r\n        firstStrike();\r\n      } else {\r\n        secondStrike();\r\n      }\r\n    }\r\n    if (ennemy.boss) {\r\n      hard++;\r\n      player.gold += moneyGeneration();\r\n      player.level++;\r\n    }\r\n    player.xp += (hard + 3);\r\n    console.log(`player xp : ${player.xp}`);\r\n    player.gold += moneyGeneration();\r\n    gainLevel();\r\n    console.log(`player pv : ${player.pv}`);\r\n    console.log(`player gold : ${player.gold}`);\r\n    console.log(`player level : ${player.level}`);\r\n    console.log(`difficulty : ${hard}`);\r\n\r\n    game[player.x][player.y].randomEvent = 'empty';\r\n    document.removeEventListener(\"keydown\", toFight);\r\n    document.addEventListener('keydown', toMove);\r\n  }\r\n}\r\n\r\nfunction gainLevel() {\r\n\r\n  if (player.xp >= 5) {\r\n    player.xp -= 5;\r\n    player.level++;\r\n    player.force += 2;\r\n    player.init++;\r\n    player.pv += hard;\r\n    console.log(`you are level ${player.level}.`);\r\n    console.log(player);\r\n  }\r\n  if (player.level === 10) {\r\n    console.log(`you have win the ${land} stage !`);\r\n    document.removeEventListener(\"keydown\", toFight);\r\n    game = \"\";\r\n    let mountain = new _level__WEBPACK_IMPORTED_MODULE_2__[\"Level\"](stage);\r\n    game = mountain.generate();\r\n    hard += 2;\r\n    player.level = 0;\r\n    player.x = 0;\r\n    player.y = 0;\r\n    land = \"mountain\";\r\n  }\r\n  if (player.level === 20) {\r\n    console.log(`you have win the ${land} stage !`);\r\n    document.removeEventListener(\"keydown\", toFight);\r\n    game = \"\";\r\n    let dungeon = new _level__WEBPACK_IMPORTED_MODULE_2__[\"Level\"](stage);\r\n    game = dungeon.generate();\r\n    hard += 2;\r\n    player.level = 0;\r\n    land = \"dungeon\";\r\n  }\r\n  if (player.level === 30) {\r\n    console.log(`you have finish the game ! GG well play !!!`);\r\n    document.removeEventListener(\"keydown\", toFight);\r\n    document.removeEventListener(\"keydown\", toMove);\r\n    console.log(`please refresh to generate a new game !`);\r\n  }\r\n}\r\n\r\nfunction armory() {\r\n  document.removeEventListener(\"keydown\", toMove);\r\n  console.log('you have find an armory.');\r\n  console.log('give 50 gold to upgrade your armory (+1).');\r\n  console.log('tape enter to spend gold or esc to leave.');\r\n  document.addEventListener(\"keydown\", forge);\r\n}\r\n\r\nfunction swordMaster() {\r\n  document.removeEventListener(\"keydown\", toMove);\r\n  console.log('you have find a sword master.');\r\n  console.log('give 100 gold to train, gain 5 in force.');\r\n  console.log('tape enter to spend gold or esc to leave.');\r\n  document.addEventListener(\"keydown\", sword);\r\n}\r\n\r\nfunction sword(event) {\r\n  if (event.keyCode === 13) {\r\n    if (player.gold >= 100) {\r\n      player.gold -= 100;\r\n      player.force += 5;\r\n      console.log('you gain 5 force.');\r\n    } else {\r\n      console.log('you have not enought gold.');\r\n    }\r\n  }\r\n  if (event.keyCode === 27) {\r\n    console.log('you leave the sword master');\r\n    document.removeEventListener(\"keydown\", sword);\r\n    document.addEventListener('keydown', toMove);\r\n  }\r\n};\r\n\r\nfunction forge(event) {\r\n  if (event.keyCode === 13) {\r\n    if (player.gold >= 50) {\r\n      player.gold -= 50;\r\n      player.armor++;\r\n      console.log('you gain 1 armor.');\r\n    } else {\r\n      console.log('you have not enought gold.');\r\n    }\r\n  }\r\n  if (event.keyCode === 27) {\r\n    console.log('you leave the armory');\r\n    document.removeEventListener(\"keydown\", forge);\r\n    document.addEventListener('keydown', toMove);\r\n  }\r\n};\r\n\r\nfunction rogueMaster() {\r\n  document.removeEventListener(\"keydown\", toMove);\r\n  console.log('you have find a rogue master.');\r\n  console.log('give 75 gold to train, gain 5 in agility.');\r\n  console.log('tape enter to spend gold or esc to leave.');\r\n  document.addEventListener(\"keydown\", rogue);\r\n}\r\n\r\nfunction rogue(event) {\r\n  if (event.keyCode === 13) {\r\n    if (player.gold >= 75) {\r\n      player.gold -= 75;\r\n      player.agility += 5;\r\n      console.log('you gain 5 agility.');\r\n    } else {\r\n      console.log('you have not enought gold.');\r\n    }\r\n  }\r\n  if (event.keyCode === 27) {\r\n    console.log('you leave the rogue master');\r\n    document.removeEventListener(\"keydown\", rogue);\r\n    document.addEventListener('keydown', toMove);\r\n  }\r\n};\r\n\r\nfunction healer(event) {\r\n  if (event.keyCode === 13) {\r\n    if (player.gold >= 15) {\r\n      player.gold -= 15;\r\n      player.pv++;\r\n      console.log('you gain 1 pv.');\r\n    } else {\r\n      console.log('you have not enought gold.');\r\n    }\r\n  }\r\n  if (event.keyCode === 27) {\r\n    console.log('you leave the thaumaturge.');\r\n    document.removeEventListener(\"keydown\", healer);\r\n    document.addEventListener('keydown', toMove);\r\n  }\r\n};\r\n\r\nfunction thaumaturge() {\r\n  document.removeEventListener(\"keydown\", toMove);\r\n  console.log('you have find a thaumaturge.');\r\n  console.log('give 15 gold to heal yourself, gain 1 pv.');\r\n  console.log('press enter to spend gold or esc to leave.');\r\n  document.addEventListener(\"keydown\", healer);\r\n}\r\n\r\nfunction moneyGeneration() {\r\n  return Math.floor((Math.random()) * 50);\r\n}\r\n\r\nfunction enygma() {\r\n  document.removeEventListener('keydown', toMove);\r\n  console.log(`You must find the answer to continue`);\r\n  let tab = [{\r\n    \"question\": \"I have 5 fingers but no bones. Who am I ?\",\r\n    \"answer\": \"glove\"\r\n  }, {\r\n    \"question\": \"How much is ((1425/6)+(3467×59867)-(9876+43)) ×0 ?\",\r\n    \"answer\": \"0\"\r\n  }, {\r\n    \"question\": \"I have a lock but no door. Who am I ?\",\r\n    \"answer\": \"padlock\"\r\n  }, {\r\n    \"question\": \"What is between EARTH and SKY ?\",\r\n    \"answer\": \"and\"\r\n  }, {\r\n    \"question\": \"Find the errror : 123456789\",\r\n    \"answer\": \"r\"\r\n  }, {\r\n    \"question\": \"One man has 7 daughters, each daugther has one brother. How many children does he have ?\",\r\n    \"answer\": \"8\"\r\n  }];\r\n\r\n  let person = \"\";\r\n  let choice = Math.floor(Math.random() * 6);\r\n  while (true) {\r\n    let question = tab[choice].question;\r\n    let answer = tab[choice].answer;\r\n    person = prompt(`Here's an enigma :\\n${question}`);\r\n    if (person === answer) {\r\n      game[player.x][player.y].randomEvent = 'empty';\r\n      let success = 4 + hard\r\n      player.xp += success;\r\n      console.log(`you have won ${success} xp.`)\r\n      document.addEventListener('keydown', toMove);\r\n      gainLevel();\r\n      break;\r\n    }\r\n  }\r\n}\r\n\r\nfunction treasure() {\r\n  let treasure = moneyGeneration();\r\n  player.gold += treasure;\r\n  console.log(`you have find ${treasure} gold.`);\r\n  gainLevel();\r\n  game[player.x][player.y].randomEvent = 'empty';\r\n}\r\n\r\nfunction trap() {\r\n  let test = Math.floor(Math.random() * 101);\r\n  if (test > player.agility) {\r\n    console.log(`you fail agility test with ${test}. You are trapped ! Loose ${hard} pv.`);\r\n    player.pv -= hard;\r\n    player.death();\r\n  } else {\r\n    console.log(`you disable a trap. you win ${hard} xp.`);\r\n    player.xp += hard;\r\n    gainLevel();\r\n  }\r\n  game[player.x][player.y].randomEvent = 'empty';\r\n}\r\n\r\nfunction trapppedTreasure() {\r\n  let test = Math.floor(Math.random() * 101);\r\n  if (player.agility < test) {\r\n    let bingo = moneyGeneration();\r\n    console.log(`You are trapped with ${test}/${player.agility} ! Loose ${hard} pv but you find a treasure : ${bingo} gold.`);\r\n    player.gold += bingo;\r\n    player.pv -= hard;\r\n    player.death();\r\n  } else {\r\n    let bingo = moneyGeneration();\r\n    player.gold += bingo;\r\n    player.xp += hard;\r\n    console.log(`you disable a trap with ${test}/${player.agility}.you have win ${bingo} gold and ${hard} xp.`);\r\n  };\r\n  gainLevel();\r\n  game[player.x][player.y].randomEvent = 'empty';\r\n}\r\n\r\nfunction nameMobGenerator() {\r\n  if (land === \"wood\") {\r\n    return 'goblin';\r\n  }\r\n  if (land === \"mountain\") {\r\n    return 'mountain savage';\r\n  }\r\n  if (land === \"dungeon\") {\r\n    return 'dark servitor';\r\n  }\r\n}\r\n\r\nfunction nameBossGenerator() {\r\n  if (land === \"wood\") {\r\n    return 'goblin tribal chief';\r\n  }\r\n  if (land === \"mountain\") {\r\n    return 'White Troll';\r\n  }\r\n  if (land === \"dungeon\") {\r\n    return 'Azazel demon';\r\n  }\r\n}\r\n\r\nfunction drawGrid(grid) {  \r\n  let tab = document.createElement('table');\r\n  let section = document.querySelector('#board');\r\n  console.log(section);\r\n  section.appendChild(tab);\r\n  for (const ligne of grid) {\r\n    let tr = document.createElement('tr');\r\n    tab.appendChild(tr);\r\n    for (const room of ligne) {\r\n      let td = document.createElement('td');\r\n      if (player.x === room.x && player.y === room.y) {\r\n        td.appendChild(player.toDraw());\r\n      }\r\n      tr.appendChild(td);\r\n    }\r\n  }\r\n}\r\n\r\n\r\n\r\nconsole.log('press any key to enter into the dungeon');\r\n\r\ndocument.addEventListener('keydown', start);\r\n\r\ndocument.addEventListener('keydown', toMove);\r\n\n\n//# sourceURL=webpack:///./src/js/game.js?");

/***/ }),

/***/ "./src/js/level.js":
/*!*************************!*\
  !*** ./src/js/level.js ***!
  \*************************/
/*! exports provided: Level */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Level\", function() { return Level; });\n/* harmony import */ var _room__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./room */ \"./src/js/room.js\");\n\r\n\r\n\"use strict\";\r\n\r\nclass Level {\r\n  constructor(level) {\r\n    this.level = level;\r\n  }\r\n  generate() {\r\n    let grid = [];\r\n    let board = document.querySelector(\"#board\");\r\n    for (let x = 0; x < this.level; x++) {\r\n      let line = document.createElement(\"tr\");\r\n      board.appendChild(line);\r\n      grid[x] = [];\r\n      for (let y = 0; y < this.level; y++) {\r\n        let drawRoom = document.createElement('td');\r\n        line.appendChild(drawRoom);\r\n        let room = grid[x][y];\r\n        room = new _room__WEBPACK_IMPORTED_MODULE_0__[\"Room\"](x, y);\r\n        room.randomEvent = room.eventGeneration();\r\n        grid[x].push(room);\r\n      };\r\n    }\r\n    return grid;\r\n  }\r\n}\r\n\n\n//# sourceURL=webpack:///./src/js/level.js?");

/***/ }),

/***/ "./src/js/mob.js":
/*!***********************!*\
  !*** ./src/js/mob.js ***!
  \***********************/
/*! exports provided: Mob */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Mob\", function() { return Mob; });\n\r\n\r\nclass Mob {\r\n  constructor(name, pv, force, init, dg,armor) {\r\n\r\n    this.name = name;\r\n    this.pv = pv;\r\n    this.force = force ;\r\n    this.init = init;\r\n    this.dg = dg;\r\n    this.armor = armor;\r\n    this.boss = false;\r\n  }\r\n\r\n  mobDeath() {\r\n    if (this.pv <= 0) {\r\n      console.log(`you have killed this ${this.name} !`);\r\n      \r\n    }\r\n  }\r\n}\r\n\n\n//# sourceURL=webpack:///./src/js/mob.js?");

/***/ }),

/***/ "./src/js/player.js":
/*!**************************!*\
  !*** ./src/js/player.js ***!
  \**************************/
/*! exports provided: Player */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Player\", function() { return Player; });\n\r\n\r\n\r\nclass Player {\r\n  constructor(name, x, y, pv, force, init, armor, power, xp, level = 0, gold, inventory = [], agility, localisation) {\r\n\r\n    this.name = name;\r\n    this.x = x;\r\n    this.y = y;\r\n    this.pv = pv;\r\n    this.force = force;\r\n    this.agility = agility;\r\n    this.init = init;\r\n    this.power = power;\r\n    this.armor = armor;\r\n    this.xp = xp;\r\n    this.level = level;\r\n    this.gold = gold;\r\n    this.inventory = inventory;\r\n    this.localisation = localisation - 1;\r\n  }\r\n  moveUp() {\r\n    this.x--;\r\n    this.toDraw();\r\n    if (this.x < 0) {\r\n      console.log('you cannot move to this direction.');\r\n      this.x = 0;\r\n    }\r\n  }\r\n  moveDown() {\r\n    this.x++;\r\n    this.toDraw();\r\n    if (this.x >= this.localisation) {\r\n      console.log('you cannot move to this direction.');\r\n      this.x = this.localisation;\r\n    }\r\n  }\r\n  moveLeft() {\r\n    this.y--;\r\n    this.toDraw();\r\n    if (this.y < 0) {\r\n      console.log('you cannot move to this direction.');\r\n      this.y = 0;\r\n    }\r\n  }\r\n  moveRight() {\r\n    this.y++;\r\n    this.toDraw();\r\n    if (this.y >= this.localisation) {\r\n      console.log('you cannot move to this direction.');\r\n      this.y = this.localisation;\r\n    }\r\n  }\r\n  totalArmor() {\r\n    return this.armor;\r\n  }\r\n  totalAttack() {\r\n    return this.power;\r\n  }\r\n  death() {\r\n    if (this.pv <= 0) {\r\n      console.log(`you haved been killed !`);\r\n      player = \"\";\r\n      document.removeEventListener(\"keydown\", toFight);\r\n    }\r\n  }\r\n  toDraw() {\r\n    board.innerHTML = \"\";\r\n    let playerVisual = document.createElement(\"div\");\r\n    playerVisual.style.position = \"absolute\";\r\n    playerVisual.style.height = `${25}px`;\r\n    playerVisual.style.width = `${25}px`;\r\n    playerVisual.style.backgroundColor = \"black\";\r\n    playerVisual.setAttribute(\"id\", \"me\");\r\n    board.appendChild(playerVisual);\r\n    playerVisual.style.top = `${this.x * 50}px`;\r\n    playerVisual.style.left = `${this.y * 50}px`;\r\n  }\r\n}\r\n\n\n//# sourceURL=webpack:///./src/js/player.js?");

/***/ }),

/***/ "./src/js/room.js":
/*!************************!*\
  !*** ./src/js/room.js ***!
  \************************/
/*! exports provided: Room */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Room\", function() { return Room; });\n\r\n\r\nclass Room {\r\n  constructor(x, y, ground = [], randomEvent) {\r\n    this.x = x;\r\n    this.y = y;\r\n    this.ground = ground;\r\n    this.randomEvent = randomEvent;\r\n  }\r\n\r\n  trader() {\r\n    let i = ['armory', 'sword master', 'rogue master', 'thaumaturge'];\r\n    return i[Math.floor(Math.random() * i.length)]\r\n  }\r\n\r\n  eventGeneration() {\r\n    let tab = ['empty', 'ennemy', 'enygma', 'treasure', 'ennemy', 'trap', 'trapped-treasure', 'ennemy', 'boss', 'trader'];\r\n    let random = tab[Math.floor(Math.random() * tab.length)];\r\n    if (random === 'trader') {\r\n      let trade = this.trader()\r\n      if (trade === 'armory') {\r\n        random = 'armory';\r\n      } else if (trade === 'sword master') {\r\n        random = 'sword master';\r\n      } else if (trade === 'rogue master') {\r\n        random = 'rogue master';\r\n      } else if (trade === 'thaumaturge') {\r\n        random = 'thaumaturge';\r\n      }\r\n    }\r\n    return random;\r\n  }\r\n}\r\n\r\n\n\n//# sourceURL=webpack:///./src/js/room.js?");

/***/ })

/******/ });