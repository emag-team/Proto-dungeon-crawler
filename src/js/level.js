import { Room } from './room'

"use strict";

export class Level {
  constructor(level) {
    this.level = level;
  }
  generate() {
    let grid = [];
    let board = document.querySelector("#board");
    for (let x = 0; x < this.level; x++) {
      let line = document.createElement("tr");
      board.appendChild(line);
      grid[x] = [];
      for (let y = 0; y < this.level; y++) {
        let drawRoom = document.createElement('td');
        line.appendChild(drawRoom);
        let room = grid[x][y];
        room = new Room(x, y);
        room.randomEvent = room.eventGeneration();
        grid[x].push(room);
      };
    }
    return grid;
  }
}
